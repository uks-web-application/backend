package com.web.uks.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHelper {

    public static <T> ResponseOk<T> ok(T data) {
        ResponseOk<T> response = new ResponseOk<T>();
        response.setMessage("Success");
        response.setStatus("200");
        response.setData(data);
        return response;
    }

    public static <T> ResponseEntity<Response<T>> errorResponse(HttpStatus status, T error) {
        Response<T> response = new Response<>();
        response.setStatus(String.valueOf(status.value()));
        response.setMessage(status.name());
        response.setError(error);
        return new ResponseEntity<>(response, status);
    }
}
