package com.web.uks.response;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Response<T> {
    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("error")
    private T error;

    public Response() {
    }

    public Response(String status, String message, T error) {
        this.status = status;
        this.message = message;
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getError() {
        return error;
    }

    public void setError(T error) {
        this.error = error;
    }
}
