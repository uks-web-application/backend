package com.web.uks.service;

import com.web.uks.exception.NotFoundException;
import com.web.uks.model.KeteraganPasien;
import com.web.uks.repository.KeteraganPasienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeteraganPasienServiceImpl {

    @Autowired
    KeteraganPasienRepository keteraganPasienRepository;

    public List<KeteraganPasien> findAll() {
        return keteraganPasienRepository.findAll();
    }

    public KeteraganPasien findById(Long id) {
        return keteraganPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("penanganan tidak ditemukan!"));
    }

    public KeteraganPasien create(KeteraganPasien keteraganPasien) {
        return keteraganPasienRepository.save(keteraganPasien);
    }

    public KeteraganPasien update(Long id, KeteraganPasien keteraganPasien) {
        KeteraganPasien keteraganPasien1 = keteraganPasienRepository.findById(id).orElseThrow(() -> new NotFoundException("penanganan tidak ditemukan!"));
        keteraganPasien1.setName(keteraganPasien.getName());
        return keteraganPasienRepository.save(keteraganPasien1);
    }

    public void delete(Long id) {
        keteraganPasienRepository.deleteById(id);
    }
}

