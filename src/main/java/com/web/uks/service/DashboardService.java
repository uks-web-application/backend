package com.web.uks.service;

import com.web.uks.model.DaftarObat;
import com.web.uks.model.Periksa;
import com.web.uks.payload.response.DashboardResponse;
import com.web.uks.repository.DaftarObatRepository;
import com.web.uks.repository.PeriksaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DashboardService {
    private PeriksaRepository periksaRepository;
    private DaftarObatRepository daftarObatRepository;

    @Autowired
    public DashboardService(PeriksaRepository periksaRepository, DaftarObatRepository daftarObatRepository) {
        this.periksaRepository = periksaRepository;
        this.daftarObatRepository = daftarObatRepository;
    }

    @Transactional(readOnly = true)
    public DashboardResponse getTotalInformationUks() {
        List<Periksa> siswaList = periksaRepository.findAllBySiswaIdIsNotNull();
        List<Periksa> guruList = periksaRepository.findAllByGuruIdIsNotNull();
        List<Periksa> karyawanList = periksaRepository.findAllByKaryawanIdIsNotNull();
        DashboardResponse create = new DashboardResponse();
        create.setTotalPasienGuru(guruList.size());
        create.setTotalPasienSiswa(siswaList.size());
        create.setTotalPasienKaryawan(karyawanList.size());
        return create;
    }
}
