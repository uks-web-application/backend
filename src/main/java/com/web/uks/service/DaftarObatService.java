package com.web.uks.service;

import com.web.uks.exception.NotFoundException;
import com.web.uks.model.DaftarObat;
import com.web.uks.model.PenangananPertama;
import com.web.uks.payload.request.DaftarObatRequest;
import com.web.uks.repository.DaftarObatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DaftarObatService {

    private  DaftarObatRepository daftarObatRepository;

    @Autowired
    public DaftarObatService(DaftarObatRepository daftarObatRepository) {
        this.daftarObatRepository = daftarObatRepository;
    }

    @Transactional(readOnly = true)
    public List<DaftarObat> findAll() {
        return daftarObatRepository.findAll();
    }

    @Transactional(readOnly = true)
    public DaftarObat findById(Long id) {
        return daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("ID NOT FOUND"));
    }

    @Transactional
    public DaftarObat create(DaftarObatRequest daftarObat) {
        DaftarObat data = new DaftarObat();
        data.setDosis(daftarObat.getDosis());
        data.setExpired(daftarObat.getExpired());
        data.setNamaObat(daftarObat.getNamaObat());
        return daftarObatRepository.save(data);
    }

    public DaftarObat update(Long id, DaftarObatRequest daftarObatRequest) {
        DaftarObat daftarObat = daftarObatRepository.findById(id).orElseThrow(() -> new NotFoundException("ID tidak ditemukan!"));
        daftarObat.setNamaObat(daftarObatRequest.getNamaObat());
        daftarObat.setDosis(daftarObatRequest.getDosis());
        daftarObat.setExpired(daftarObatRequest.getExpired());
        return daftarObatRepository.save(daftarObat);
    }
    public void delete(Long id) {
        daftarObatRepository.deleteById(id);
    }
}
