package com.web.uks.service;

import com.web.uks.exception.BussinesException;
import com.web.uks.exception.NotFoundException;
import com.web.uks.model.*;
import com.web.uks.payload.request.PenangananPeriksaRequest;
import com.web.uks.payload.request.PeriksaRequest;
import com.web.uks.payload.response.DashboardResponse;
import com.web.uks.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class PeriksaService {

    private PeriksaRepository periksaRepository;
    private GuruRepository guruRepository;
    private SiswaRepository siswaRepository;
    private KaryawanRepository karyawanRepository;
    private KeteraganPasienRepository keteraganPasienRepository;
    private PasienStatusRepository pasienStatusRepository;
    private PenangananPertamaRepository penangananPertamaRepository;
    private DiagnosaPenyakitRepository diagnosaPenyakitRepository;
    private PenangananPeriksaRepository penangananPeriksaRepository;

    @Autowired
    public PeriksaService(PeriksaRepository periksaRepository, GuruRepository guruRepository, KeteraganPasienRepository keteraganPasienRepository, SiswaRepository siswaRepository, KaryawanRepository karyawanRepository, PenangananPertamaRepository penangananPertamaRepository, PasienStatusRepository pasienStatusRepository, DiagnosaPenyakitRepository diagnosaPenyakitRepository, PenangananPeriksaRepository penangananPeriksaRepository) {
        this.periksaRepository = periksaRepository;
        this.guruRepository = guruRepository;
        this.keteraganPasienRepository = keteraganPasienRepository;
        this.siswaRepository = siswaRepository;
        this.karyawanRepository = karyawanRepository;
        this.penangananPertamaRepository = penangananPertamaRepository;
        this.pasienStatusRepository = pasienStatusRepository;
        this.diagnosaPenyakitRepository = diagnosaPenyakitRepository;
        this.penangananPeriksaRepository = penangananPeriksaRepository;
    }

    @Transactional(readOnly = true)
    public List<Periksa> getPeriksa() {
        var data = periksaRepository.findAll(Sort.by("createAt").descending());
        List<Periksa> result = new ArrayList<>();
        for (Periksa val : data) {
            val.setListPenanganan(penangananPeriksaRepository.findAllByPeriksaId(val.getId()));
            result.add(val);
        }
        return result;
    }

    public Map<String, Object> getRekapDataSemesterGasal(Date startDate, Date endDate) {
        List<Periksa> siswaList = periksaRepository.findAllBySiswaIdIsNotNull();
        List<Periksa> guruList = periksaRepository.findAllByGuruIdIsNotNull();
        List<Periksa> karyawanList = periksaRepository.findAllByKaryawanIdIsNotNull();
        DashboardResponse create = new DashboardResponse();
        var data = periksaRepository.findByCreateAtBetween(startDate, endDate);
        List<Periksa> result = new ArrayList<>();
        for (Periksa val : data) {
            val.setListPenanganan(penangananPeriksaRepository.findAllByPeriksaId(val.getId()));
            result.add(val);
        }
        create.setTotalPasienGuru(guruList.size());
        create.setTotalPasienSiswa(siswaList.size());
        create.setTotalPasienKaryawan(karyawanList.size());

        Map<String, Object> response = new HashMap<>();
        response.put("informationUks", create);
        response.put("data", result);
        return response;
    }

    @Transactional(readOnly = true)
    public Periksa findById(Long id) {
        Periksa response = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Periksa ID NOT FOUND"));
        response.setListPenanganan(penangananPeriksaRepository.findAllByPeriksaId(response.getId()));
        return response;
    }

    public Periksa penanganan(Long id, PenangananPeriksaRequest dto) {
        Periksa data = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("ID PENANGANAN NOT FOUND"));
        if (data.isHandled()) throw new BussinesException("Pasien sudah di tangani");
        data.setKeluhan(dto.getKeluhan());
        List<PenangananPeriksa> listPenanganan = new ArrayList<>();
        if (dto.getListPenanganan().isEmpty()) throw new BussinesException("Penanganan Harus Di isi");
        for (var list : dto.getListPenanganan()) {
            PenangananPeriksa create = new PenangananPeriksa();
            create.setDiagnosaPenyakitId(diagnosaPenyakitRepository.findById((long) list.getDiagnosaPenyakitId()).orElseThrow(() -> new NotFoundException("Diagnosa Id not found")));
            create.setPenangananPertamaId(penangananPertamaRepository.findById((long) list.getPenangananPertamaId()).orElseThrow(() -> new NotFoundException("Daftar Obat Id not found")));
            create.setKeteraganPasienId(keteraganPasienRepository.findById((long) list.getKeteraganPasienId()).orElseThrow(() -> new NotFoundException("Keterangan Periksa Id not found")));
            create.setPeriksaId(data);
            create.setCatatan(list.getCatatan());
            listPenanganan.add(create);
            penangananPeriksaRepository.save(create);
        }
        data.setListPenanganan(penangananPeriksaRepository.findAllByPeriksaId(data.getId()));
        data.setHandled(true);
        return periksaRepository.save(data);
    }

    @Transactional
    public Periksa create(PeriksaRequest periksaRequest) {
        Periksa periksa = new Periksa();
        String status = periksaRequest.getStatus().trim();
        if (status.equals("Siswa")) {
            Siswa siswa = siswaRepository.findById((long) periksaRequest.getPasienId()).orElseThrow(() -> new NotFoundException("Pasien Id tidak di temukan. pasien id diambil dari data siswa"));
            siswa.setTotalPeriksa(siswa.getTotalPeriksa() + 1);
            siswaRepository.save(siswa);
            periksa.setSiswaId(siswa);
            periksa.setPasienStatusId(pasienStatusRepository.findById(2).orElseThrow(() -> new NotFoundException("Status pasien tidak ditemukan")));
            periksa.setKeluhan(periksaRequest.getKeluhan());
            periksa.setNamaPasien(periksa.getSiswaId().getNamaSiswa());
            periksa.setHandled(false);
            return periksaRepository.save(periksa);
        }
        if (status.equals("Karyawan")) {
            Karyawan karyawan = karyawanRepository.findById((long) periksaRequest.getPasienId()).orElseThrow(() -> new NotFoundException("Pasien Id tidak di temukan. pasien id diambil dari data siswa"));
            karyawan.setTotalPeriksa(karyawan.getTotalPeriksa() + 1);
            karyawanRepository.save(karyawan);
            periksa.setKaryawanId(karyawan);
            periksa.setPasienStatusId(pasienStatusRepository.findById(3).orElseThrow(() -> new NotFoundException("Status pasien tidak ditemukan")));
            periksa.setKeluhan(periksaRequest.getKeluhan());
            periksa.setNamaPasien(periksa.getKaryawanId().getNamaKaryawan());
            periksa.setHandled(false);
            return periksaRepository.save(periksa);
        }
        Guru guru = guruRepository.findById((long) periksaRequest.getPasienId()).orElseThrow(() -> new NotFoundException("Pasien Id tidak di temukan. pasien id diambil dari data guru"));
        guru.setTotalPeriksa(guru.getTotalPeriksa() + 1);
        guruRepository.save(guru);
        periksa.setGuruId(guru);
        periksa.setPasienStatusId(pasienStatusRepository.findById(1).orElseThrow(() -> new NotFoundException("Status pasien tidak ditemukan")));
        periksa.setKeluhan(periksaRequest.getKeluhan());
        periksa.setNamaPasien(periksa.getGuruId().getNamaGuru());
        periksa.setHandled(false);
        return periksaRepository.save(periksa);
    }
}
