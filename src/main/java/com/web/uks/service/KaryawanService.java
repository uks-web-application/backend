package com.web.uks.service;

import com.web.uks.model.Karyawan;
import com.web.uks.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class KaryawanService {
    @Autowired
    KaryawanRepository karyawanRepository;

    public List<Karyawan> getKaryawan() {
        return karyawanRepository.findAll();
    }

    public List<Karyawan> karyawanChecklistFrequently() {
        return karyawanRepository.findAllByTotalPeriksaDesc();
    }

    public Karyawan addKaryawan(Karyawan karyawan) {
        return karyawanRepository.save(karyawan);
    }

    public Karyawan editKaryawan(Long id, String namaKaryawan, String tempatLahir, Date tanggalLahir, String alamat) {
        Karyawan karyawan = karyawanRepository.findById(id).orElse(null);
        karyawan.setNamaKaryawan(namaKaryawan);
        karyawan.setTempatLahir(tempatLahir);
        karyawan.setTanggalLahir(tanggalLahir);
        karyawan.setAlamat(alamat);
        return karyawanRepository.save(karyawan);
    }

    public Karyawan getById(Long id) {
        return karyawanRepository.findById(id).orElse(null);
    }

    public void deleteKaryawan(Long id) {
        karyawanRepository.deleteById(id);
    }
}
