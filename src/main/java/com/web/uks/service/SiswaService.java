package com.web.uks.service;

import com.web.uks.model.Siswa;
import com.web.uks.repository.SiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SiswaService {
    @Autowired
    SiswaRepository siswaRepository;

    public List<Siswa> getSiswa() {
        return siswaRepository.findAll();
    }

    public Siswa addSiswa(Siswa siswa) {
        return siswaRepository.save(siswa);
    }

    public List<Siswa> studentChecklistFrequently() {
        return siswaRepository.findAllByTotalPeriksaDesc();
    }


    public Siswa editSiswa(Long id, String namaSiswa, String kelas, String tempatLahir, Date tanggalLahir, String alamat) {
        Siswa siswa = siswaRepository.findById(id).orElse(null);
        siswa.setNamaSiswa(namaSiswa);
        siswa.setKelas(kelas);
        siswa.setTempatLahir(tempatLahir);
        siswa.setTanggalLahir(tanggalLahir);
        siswa.setAlamat(alamat);
        return siswaRepository.save(siswa);
    }

    public Siswa getById(Long id) {
        return siswaRepository.findById(id).orElse(null);
    }

    public void deleteSiswa(Long id) {
        siswaRepository.deleteById(id);
    }
}
