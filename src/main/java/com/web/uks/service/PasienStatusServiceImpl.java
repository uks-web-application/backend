package com.web.uks.service;

import com.web.uks.model.PasienStatus;
import com.web.uks.repository.PasienStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PasienStatusServiceImpl {

    private PasienStatusRepository pasienStatusRepository;

    @Autowired
    public PasienStatusServiceImpl(PasienStatusRepository pasienStatusRepository) {
        this.pasienStatusRepository = pasienStatusRepository;
    }

    public List<PasienStatus> findAll() {
        return pasienStatusRepository.findAll();
    }

    public PasienStatus create (PasienStatus pasienStatus) {
        return pasienStatusRepository.save(pasienStatus);
    }

    public void delete(int id) {
        pasienStatusRepository.deleteById(id);
    }
}
