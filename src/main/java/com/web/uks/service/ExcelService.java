package com.web.uks.service;

import com.web.uks.exception.NotFoundException;
import com.web.uks.helper.ExcelHelper;
import com.web.uks.model.*;
import com.web.uks.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ExcelService {
    @Autowired
    SiswaRepository siswaRepository;
    @Autowired
    GuruRepository guruRepository;
    @Autowired
    KaryawanRepository karyawanRepository;
    @Autowired
    PeriksaRepository periksaRepository;
    @Autowired
    PenangananPeriksaRepository penangananPeriksaRepository;

    @Transactional(readOnly = true)
    public ByteArrayInputStream download(Long id) {
        Periksa periksa = periksaRepository.findById(id).orElseThrow(() -> new NotFoundException("Periksa ID NOT FOUND"));
        PenangananPeriksa penangananPeriksa = penangananPeriksaRepository.findByPeriksaId(periksa.getId());
        ByteArrayInputStream in = ExcelHelper.download(periksa, penangananPeriksa);
        return in;
    }

    public void saveSiswa(MultipartFile file) {
        try {
            List<Siswa> siswaList = ExcelHelper.excelToSiswa(file.getInputStream());
            siswaRepository.saveAll(siswaList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public void saveGuru(MultipartFile file) {
        try {
            List<Guru> gurus = ExcelHelper.excelToGuru(file.getInputStream());
            guruRepository.saveAll(gurus);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public void saveKaryawan(MultipartFile file) {
        try {
            List<Karyawan> karyawans = ExcelHelper.excelToKaryawan(file.getInputStream());
            karyawanRepository.saveAll(karyawans);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }

    public ByteArrayInputStream downloadDataGuru() {
        List<Guru> guru = guruRepository.findAll();
        ByteArrayInputStream in = ExcelHelper.guruToExcel(guru);
        return in;
    }

    public ByteArrayInputStream downloadDataSiswa() {
        List<Siswa> siswa = siswaRepository.findAll();
        ByteArrayInputStream in = ExcelHelper.siswaToExcel(siswa);
        return in;
    }

    public ByteArrayInputStream downloadDataKaryawan() {
        List<Karyawan> karyawan = karyawanRepository.findAll();
        ByteArrayInputStream in = ExcelHelper.karyawanToExcel(karyawan);
        return in;
    }



}
