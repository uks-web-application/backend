package com.web.uks.service;

import com.web.uks.model.Guru;
import com.web.uks.repository.GuruRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class GuruService {
    @Autowired
    GuruRepository guruRepository;

    public List<Guru> getGuru() {
        return guruRepository.findAll();
    }

    public List<Guru> teacherChecklistFrequently() {
        return guruRepository.findAllByTotalPeriksaDesc();
    }

    public Guru addGuru(Guru guru) {
        return guruRepository.save(guru);
    }

    public Guru editGuru(Long id, String namaGuru, String tempatLahir, Date tanggalLahir, String alamat) {
        Guru guru = guruRepository.findById(id).orElse(null);
        guru.setNamaGuru(namaGuru);
        guru.setTempatLahir(tempatLahir);
        guru.setTanggalLahir(tanggalLahir);
        guru.setAlamat(alamat);
        return guruRepository.save(guru);
    }

    public Guru getById(Long id) {
        return guruRepository.findById(id).orElse(null);
    }

    public void deleteGuru(Long id) {
        guruRepository.deleteById(id);
    }
}
