package com.web.uks.service;

import com.web.uks.exception.NotFoundException;
import com.web.uks.model.DiagnosaPenyakit;
import com.web.uks.repository.DiagnosaPenyakitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DiagnosaPenyakitServiceImpl {

    @Autowired
    private DiagnosaPenyakitRepository diagnosaPenyakitRepository;

    public List<DiagnosaPenyakit> findAll() {
        return diagnosaPenyakitRepository.findAll();
    }

    public DiagnosaPenyakit findById(Long id) {
        return diagnosaPenyakitRepository.findById(id).orElseThrow(() -> new NotFoundException("diagnosa tidak ditemukan!"));
    }

    public DiagnosaPenyakit create(DiagnosaPenyakit diagnosaPenyakit) {
        return diagnosaPenyakitRepository.save(diagnosaPenyakit);
    }

    @Transactional
    public DiagnosaPenyakit update(Long id, DiagnosaPenyakit diagnosaPenyakit) {
        DiagnosaPenyakit diagnosaPenyakit1 = diagnosaPenyakitRepository.findById(id).orElseThrow(() -> new NotFoundException("Diagnosa penyakit tidak ditemukan!"));
        diagnosaPenyakit1.setName(diagnosaPenyakit.getName());
        return diagnosaPenyakitRepository.save(diagnosaPenyakit1);
    }

    public void delete(Long id) {
        diagnosaPenyakitRepository.deleteById(id);
    }
}
