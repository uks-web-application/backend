package com.web.uks.service;

import com.web.uks.exception.NotFoundException;
import com.web.uks.model.PenangananPertama;
import com.web.uks.repository.PenangananPertamaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenangananPertamaService {
    @Autowired
    PenangananPertamaRepository penangananPertamaRepository;

    public List<PenangananPertama> findAll() {
        return penangananPertamaRepository.findAll();
    }

    public PenangananPertama findById(Long id) {
        return penangananPertamaRepository.findById(id).orElseThrow(() -> new NotFoundException("penanganan tidak ditemukan!"));
    }

    public PenangananPertama create(PenangananPertama penangananPertama) {
        return penangananPertamaRepository.save(penangananPertama);
    }

    public PenangananPertama update(Long id, PenangananPertama penangananPertama) {
        PenangananPertama penangananPertama1 = penangananPertamaRepository.findById(id).orElseThrow(() -> new NotFoundException("penanganan tidak ditemukan!"));
        penangananPertama1.setNamaPenanganan(penangananPertama.getNamaPenanganan());
        return penangananPertamaRepository.save(penangananPertama1);
    }

    public void delete(Long id) {
        penangananPertamaRepository.deleteById(id);
    }
}
