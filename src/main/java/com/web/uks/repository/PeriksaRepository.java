package com.web.uks.repository;

import com.web.uks.model.Periksa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface PeriksaRepository extends JpaRepository<Periksa, Long> {
    List<Periksa> findAllBySiswaIdIsNotNull();
    List<Periksa> findAllByGuruIdIsNotNull();
    List<Periksa> findAllByKaryawanIdIsNotNull();

    List<Periksa> findByCreateAtBetween(Date startDate, Date endDate);
}
