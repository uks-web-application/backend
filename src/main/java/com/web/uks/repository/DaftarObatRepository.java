package com.web.uks.repository;

import com.web.uks.model.DaftarObat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DaftarObatRepository extends JpaRepository<DaftarObat, Long> {
}
