package com.web.uks.repository;

import com.web.uks.model.Karyawan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface KaryawanRepository extends JpaRepository<Karyawan, Long> {
    @Query(value = "SELECT a.* FROM karyawan a ORDER BY a.total_periksa DESC LIMIT 5", nativeQuery = true)
    List<Karyawan> findAllByTotalPeriksaDesc();
}
