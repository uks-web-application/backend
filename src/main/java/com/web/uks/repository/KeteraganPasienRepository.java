package com.web.uks.repository;

import com.web.uks.model.KeteraganPasien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KeteraganPasienRepository extends JpaRepository<KeteraganPasien, Long> {
}
