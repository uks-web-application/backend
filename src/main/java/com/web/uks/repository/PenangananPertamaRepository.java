package com.web.uks.repository;

import com.web.uks.model.PenangananPertama;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PenangananPertamaRepository extends JpaRepository<PenangananPertama, Long> {
}
