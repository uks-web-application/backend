package com.web.uks.repository;

import com.web.uks.model.DiagnosaPenyakit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiagnosaPenyakitRepository extends JpaRepository<DiagnosaPenyakit, Long> {
}
