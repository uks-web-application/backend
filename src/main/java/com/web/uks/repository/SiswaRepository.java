package com.web.uks.repository;

import com.web.uks.model.Siswa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SiswaRepository extends JpaRepository<Siswa, Long> {

    @Query(value = "SELECT a.* FROM siswa a ORDER BY a.total_periksa DESC LIMIT 5", nativeQuery = true)
    List<Siswa> findAllByTotalPeriksaDesc();
}
