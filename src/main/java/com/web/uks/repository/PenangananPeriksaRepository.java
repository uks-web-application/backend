package com.web.uks.repository;

import com.web.uks.model.PenangananPeriksa;
import com.web.uks.model.Periksa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PenangananPeriksaRepository extends JpaRepository<PenangananPeriksa, Integer> {

    @Query(value = "SELECT a.* FROM penanganan_periksa a WHERE a.periksa_id = :periksaId", nativeQuery = true)
    List<PenangananPeriksa> findAllByPeriksaId(Long periksaId);

    @Query(value = "SELECT a.* FROM penanganan_periksa a WHERE a.periksa_id = :periksaId", nativeQuery = true)
    PenangananPeriksa findByPeriksaId(Long periksaId);
}
