package com.web.uks.repository;

import com.web.uks.model.Guru;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GuruRepository extends JpaRepository<Guru, Long> {
    @Query(value = "SELECT a.* FROM guru a ORDER BY a.total_periksa DESC LIMIT 5", nativeQuery = true)
    List<Guru> findAllByTotalPeriksaDesc();
}
