package com.web.uks.repository;

import com.web.uks.model.PasienStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasienStatusRepository extends JpaRepository<PasienStatus, Integer> {

}
