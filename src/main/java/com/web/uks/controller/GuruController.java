package com.web.uks.controller;

import com.web.uks.model.Guru;
import com.web.uks.service.GuruService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/guru")
public class GuruController {
    @Autowired
    GuruService guruService;

    @RequestMapping(value = "/guru", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        List<Guru> guru = guruService.getGuru();
        return new ResponseEntity<>(guru, HttpStatus.OK);
    }

    @GetMapping("/checklist-frequently")
    public ResponseEntity<List<Guru>> teacherChecklistFrequently() {
        return new ResponseEntity<>(guruService.teacherChecklistFrequently(), HttpStatus.OK);
    }

    @RequestMapping(value = "/guru", method = RequestMethod.POST)
    public ResponseEntity<?> addGuru(@RequestBody Guru guru) {
        Guru guru1 = guruService.addGuru(guru);
        return new ResponseEntity<>(guru1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/guru/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        Guru guru = guruService.getById(id);
        return new ResponseEntity<>(guru, HttpStatus.OK);
    }
    @RequestMapping(value = "/guru/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> editGuru(@PathVariable("id") Long id, @RequestBody Guru guru) {
        Guru guru1 = guruService.editGuru(id, guru.getNamaGuru(), guru.getTempatLahir(), guru.getTanggalLahir(), guru.getAlamat());
        return new ResponseEntity<>(guru1, HttpStatus.OK);
    }

    @RequestMapping(value = "/guru/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGuru(@PathVariable("id") Long id) {
        guruService.deleteGuru(id);
        return new ResponseEntity<>("Delete success! ", HttpStatus.OK);
    }
}
