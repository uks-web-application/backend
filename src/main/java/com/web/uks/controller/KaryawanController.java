package com.web.uks.controller;

import com.web.uks.model.Karyawan;
import com.web.uks.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {
    @Autowired
    KaryawanService karyawanService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        List<Karyawan> karyawan = karyawanService.getKaryawan();
        return new ResponseEntity<>(karyawan, HttpStatus.OK);
    }

    @GetMapping("/checklist-frequently")
    public ResponseEntity<List<Karyawan>> karyawanChecklistFrequently() {
        return new ResponseEntity<>(karyawanService.karyawanChecklistFrequently(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addKaryawan(@RequestBody Karyawan karyawan) {
        Karyawan karyawan1 = karyawanService.addKaryawan(karyawan);
        return new ResponseEntity<>(karyawan1, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        Karyawan karyawan = karyawanService.getById(id);
        return new ResponseEntity<>(karyawan, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> editKaryawan(@PathVariable("id") Long id, @RequestBody Karyawan karyawan) {
        Karyawan karyawan1 = karyawanService.editKaryawan(id, karyawan.getNamaKaryawan(), karyawan.getTempatLahir(), karyawan.getTanggalLahir(), karyawan.getAlamat());
        return new ResponseEntity<>(karyawan1, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteKaryawan(@PathVariable("id") Long id) {
        karyawanService.deleteKaryawan(id);
        return new ResponseEntity<>("Delete success! ", HttpStatus.OK);
    }
}
