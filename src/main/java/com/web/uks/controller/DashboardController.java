package com.web.uks.controller;

import com.web.uks.payload.response.DashboardResponse;
import com.web.uks.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {
    @Autowired
    private DashboardService dashboardService;

    @GetMapping
    public DashboardResponse getTotalInformationUks() {
        return dashboardService.getTotalInformationUks();
    }
}
