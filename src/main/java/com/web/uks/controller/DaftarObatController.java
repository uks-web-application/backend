package com.web.uks.controller;

import com.web.uks.model.DaftarObat;
import com.web.uks.payload.request.DaftarObatRequest;
import com.web.uks.service.DaftarObatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/daftar-obat")
public class DaftarObatController {
    @Autowired
    DaftarObatService daftarObatService;
    @GetMapping
    public List<DaftarObat> getAll() {
        return daftarObatService.findAll();
    }

    @PostMapping
    public DaftarObat create(@RequestBody DaftarObatRequest daftarObat) {
        return daftarObatService.create(daftarObat);
    }

    @GetMapping("/{id}")
    public DaftarObat findById(@PathVariable("id") Long id) {
        return daftarObatService.findById(id);
    }

    @PutMapping("/{id}")
    public DaftarObat update(@PathVariable("id") Long id, @RequestBody DaftarObatRequest daftarObat) {
        return daftarObatService.update(id, daftarObat);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        daftarObatService.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }

}
