package com.web.uks.controller;

import com.web.uks.model.PasienStatus;
import com.web.uks.service.PasienStatusServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/pasien-status")
public class PasienStatusController {

    private PasienStatusServiceImpl pasienStatusService;

    @Autowired
    public PasienStatusController(PasienStatusServiceImpl pasienStatusService) {
        this.pasienStatusService = pasienStatusService;
    }

    @GetMapping
    public ResponseEntity<List<PasienStatus>> findAll() {
        return new ResponseEntity<>(pasienStatusService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PasienStatus> create(@RequestBody PasienStatus pasienStatus) {
        return new ResponseEntity<>(pasienStatusService.create(pasienStatus), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") int id) {
        pasienStatusService.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
