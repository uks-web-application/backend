package com.web.uks.controller;

import com.web.uks.model.Admin;
import com.web.uks.payload.request.LoginRequest;
import com.web.uks.payload.request.SignupRequest;
import com.web.uks.payload.response.JwtResponse;
import com.web.uks.payload.response.MessageResponse;
import com.web.uks.repository.AdminRepository;
import com.web.uks.security.jwt.JwtUtils;
import com.web.uks.service.AdminDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin( origins = "http://localhost:3000")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateAdmin(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        AdminDetailsImpl userDetails = (AdminDetailsImpl) authentication.getPrincipal();
        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername()
        ));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (adminRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Kesalahan: Username telah digunakan!"));
        }
        // Create new user's account
        Admin admin = new Admin(signUpRequest.getUsername(), encoder.encode(signUpRequest.getPassword()));
        adminRepository.save(admin);
        return ResponseEntity.ok(new MessageResponse(" Register telah berhasil!"));
    }

}
