package com.web.uks.controller;

import com.web.uks.model.KeteraganPasien;
import com.web.uks.service.KeteraganPasienServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/keterangan-pasien")
public class keteranganPasienController {

    @Autowired
    private KeteraganPasienServiceImpl keteraganPasienService;

    @GetMapping
    public ResponseEntity<List<KeteraganPasien>> findAll() {
        return new ResponseEntity<>(keteraganPasienService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public KeteraganPasien findById(@PathVariable("id") Long id) {
        return keteraganPasienService.findById(id);
    }
    @PostMapping
    public ResponseEntity<KeteraganPasien> create(@RequestBody KeteraganPasien keteraganPasien) {
        return new ResponseEntity<>(keteraganPasienService.create(keteraganPasien), HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<KeteraganPasien> update(@PathVariable("id") Long id, @RequestBody KeteraganPasien keteraganPasien) {
        return new ResponseEntity<>(keteraganPasienService.update(id, keteraganPasien), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        keteraganPasienService.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
