package com.web.uks.controller;

import com.web.uks.model.PenangananPertama;
import com.web.uks.service.PenangananPertamaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/penanganan-pertama")
public class PenangananPertamanController {
    @Autowired
    PenangananPertamaService penangananPertamaService;

    @GetMapping
    public ResponseEntity<List<PenangananPertama>> findAll() {
        return new ResponseEntity<>(penangananPertamaService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public PenangananPertama findById(@PathVariable("id") Long id) {
        return penangananPertamaService.findById(id);
    }
    @PostMapping
    public ResponseEntity<PenangananPertama> create(@RequestBody PenangananPertama penangananPertama) {
        return new ResponseEntity<>(penangananPertamaService.create(penangananPertama), HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<PenangananPertama> update(@PathVariable("id") Long id, @RequestBody PenangananPertama penangananPertama) {
        return new ResponseEntity<>(penangananPertamaService.update(id, penangananPertama), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        penangananPertamaService.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
