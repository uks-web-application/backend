package com.web.uks.controller;

import com.web.uks.model.DiagnosaPenyakit;
import com.web.uks.service.DiagnosaPenyakitServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/diagnosa-penyakit")
public class DiagnosaPenyakitController {

    private DiagnosaPenyakitServiceImpl diagnosaPenyakitService;

    @Autowired
    public DiagnosaPenyakitController(DiagnosaPenyakitServiceImpl diagnosaPenyakitService) {
        this.diagnosaPenyakitService = diagnosaPenyakitService;
    }

    @GetMapping
    public ResponseEntity<List<DiagnosaPenyakit>> findAll() {
        return new ResponseEntity<>(diagnosaPenyakitService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public DiagnosaPenyakit findById(@PathVariable("id") Long id) {
        return diagnosaPenyakitService.findById(id);
    }
    @PostMapping
    public ResponseEntity<DiagnosaPenyakit> create(@RequestBody DiagnosaPenyakit diagnosaPenyakit) {
        return new ResponseEntity<>(diagnosaPenyakitService.create(diagnosaPenyakit), HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    public ResponseEntity<DiagnosaPenyakit> update(@PathVariable("id") Long id, @RequestBody DiagnosaPenyakit diagnosaPenyakit) {
        return new ResponseEntity<>(diagnosaPenyakitService.update(id, diagnosaPenyakit), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        diagnosaPenyakitService.delete(id);
        return new ResponseEntity<>("Deleted", HttpStatus.OK);
    }
}
