package com.web.uks.controller;

import com.web.uks.model.Periksa;
import com.web.uks.payload.request.PenangananPeriksaRequest;
import com.web.uks.payload.request.PeriksaRequest;
import com.web.uks.response.ResponseHelper;
import com.web.uks.response.ResponseOk;
import com.web.uks.service.PeriksaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/periksa")
public class PeriksaController {
    @Autowired
    PeriksaService periksaService;

    @GetMapping
    public ResponseEntity<List<Periksa>> getAll() {
        List<Periksa> periksa = periksaService.getPeriksa();
        return new ResponseEntity<>(periksa, HttpStatus.OK);
    }

    @GetMapping("/rekap-data/semester-gasal")
    public ResponseOk<Map<String, Object>> getRekapDataSemesterGasal(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate, @RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {
        return ResponseHelper.ok(periksaService.getRekapDataSemesterGasal(startDate, endDate));
    }

    @GetMapping("/{id}")
    public Periksa findById(@PathVariable("id") Long id) {
        return periksaService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Periksa> create(@RequestBody PeriksaRequest periksaRequest) {
        return new ResponseEntity<>(periksaService.create(periksaRequest), HttpStatus.CREATED);
    }

    @PutMapping("/penanganan/{id}")
    public ResponseEntity<Periksa> penanganan(@PathVariable("id") Long id, @RequestBody PenangananPeriksaRequest periksaRequest) {
        return new ResponseEntity<>(periksaService.penanganan(id, periksaRequest), HttpStatus.CREATED);
    }
}
