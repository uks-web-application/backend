package com.web.uks.controller;

import com.web.uks.model.Siswa;
import com.web.uks.service.SiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/siswa")
public class SiswaController {
    @Autowired
    SiswaService siswaService;

    @RequestMapping(value = "/siswa", method = RequestMethod.GET)
    public  List<Siswa> getAll() {
        return siswaService.getSiswa();
    }

    @GetMapping("/checklist-frequently")
    public List<Siswa> studentChecklistFrequently() {
        return siswaService.studentChecklistFrequently();
    }

    @RequestMapping(value = "/siswa", method = RequestMethod.POST)
    public Siswa addSiswa(@RequestBody Siswa siswa) {
        return siswaService.addSiswa(siswa);
    }

    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.GET)
    public Siswa getById(@PathVariable("id") Long id) {
        return siswaService.getById(id);
    }
    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.PUT)
    public Siswa editSiswa(@PathVariable("id") Long id, @RequestBody Siswa siswa) {
        return siswaService.editSiswa(id, siswa.getNamaSiswa(), siswa.getKelas(), siswa.getTempatLahir(), siswa.getTanggalLahir(), siswa.getAlamat());
    }

    @RequestMapping(value = "/siswa/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteSiswa(@PathVariable("id") Long id) {
        siswaService.deleteSiswa(id);
        return new ResponseEntity<>("Delete success! ", HttpStatus.OK);
    }
}
