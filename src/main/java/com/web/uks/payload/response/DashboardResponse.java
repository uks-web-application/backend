package com.web.uks.payload.response;

public class DashboardResponse {
    private int totalPasienSiswa;
    private int totalPasienGuru;
    private int totalPasienKaryawan;

    public int getTotalPasienSiswa() {
        return totalPasienSiswa;
    }

    public void setTotalPasienSiswa(int totalPasienSiswa) {
        this.totalPasienSiswa = totalPasienSiswa;
    }

    public int getTotalPasienGuru() {
        return totalPasienGuru;
    }

    public void setTotalPasienGuru(int totalPasienGuru) {
        this.totalPasienGuru = totalPasienGuru;
    }

    public int getTotalPasienKaryawan() {
        return totalPasienKaryawan;
    }

    public void setTotalPasienKaryawan(int totalPasienKaryawan) {
        this.totalPasienKaryawan = totalPasienKaryawan;
    }
}
