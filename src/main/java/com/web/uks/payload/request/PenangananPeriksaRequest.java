package com.web.uks.payload.request;

import java.util.ArrayList;
import java.util.List;

public class PenangananPeriksaRequest {
    private String keluhan;
    private List<ListPeriksaPenangananRequest> listPenanganan = new ArrayList<>();

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public List<ListPeriksaPenangananRequest> getListPenanganan() {
        return listPenanganan;
    }

    public void setListPenanganan(List<ListPeriksaPenangananRequest> listPenanganan) {
        this.listPenanganan = listPenanganan;
    }
}
