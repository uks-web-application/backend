package com.web.uks.payload.request;

public class DiagnostikPenyakitRequest {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
