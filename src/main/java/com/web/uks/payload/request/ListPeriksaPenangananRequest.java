package com.web.uks.payload.request;

import javax.persistence.Column;

public class ListPeriksaPenangananRequest {

    private int diagnosaPenyakitId;
    private int keteraganPasienId;
    private int penangananPertamaId;

    @Column(name = "takaran")
    private String catatan;

    public int getDiagnosaPenyakitId() {
        return diagnosaPenyakitId;
    }

    public void setDiagnosaPenyakitId(int diagnosaPenyakitId) {
        this.diagnosaPenyakitId = diagnosaPenyakitId;
    }

    public int getKeteraganPasienId() {
        return keteraganPasienId;
    }

    public void setKeteraganPasienId(int keteraganPasienId) {
        this.keteraganPasienId = keteraganPasienId;
    }

    public int getPenangananPertamaId() {
        return penangananPertamaId;
    }

    public void setPenangananPertamaId(int penangananPertamaId) {
        this.penangananPertamaId = penangananPertamaId;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
