package com.web.uks.helper;

import com.web.uks.model.*;
import com.web.uks.payload.request.ListPeriksaPenangananRequest;
import com.web.uks.repository.PenangananPeriksaRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ExcelHelper {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    static String[] HEADER_SISWA = {"nama siswa", "kelas", "tempat lahir", "tanggal lahir", "alamat"};
    static String[] HEADER_KARYAWAN = {"nama karyawan", "tempat lahir", "tanggal lahir", "alamat"};
    static String[] HEADER_GURU = {"nama guru", "tempat lahir", "tanggal lahir", "alamat"};

    static String[] HEADER_PERIKSA = {"nama pasien", "Status Pasien", "Jabatan", "Tindakan", "Tanggal", "Status", "Catatan"};
    static String SHEET = "Sheet1";

    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    public static ByteArrayInputStream download(Periksa periksaList, PenangananPeriksa penangananPeriksa) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_PERIKSA.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_PERIKSA[col]);
            }

            int rowIdx = 1;
            Row row = sheet.createRow(rowIdx);

            row.createCell(0).setCellValue(periksaList.getNamaPasien());
            row.createCell(1).setCellValue(periksaList.getPasienStatusId().getName());
            if (periksaList.getSiswaId() != null) {
                row.createCell(2).setCellValue(periksaList.getSiswaId().getKelas());
            } else if (periksaList.getGuruId() != null) {
                row.createCell(2).setCellValue("Guru");
            } else {
                row.createCell(2).setCellValue("Karyawan");
            }
            row.createCell(3).setCellValue(penangananPeriksa.getKeteraganPasienId().getName());
            row.createCell(4).setCellValue(periksaList.getCreateAt());
            if (periksaList.isHandled()) {
                row.createCell(5).setCellValue("Sudah Ditangani");
            } else {
                row.createCell(5).setCellValue("Belum Ditangani");
            }
            row.createCell(6).setCellValue(penangananPeriksa.getCatatan());

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    public static List<Siswa> excelToSiswa(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Siswa> siswaList = new ArrayList<Siswa>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Siswa siswa = new Siswa();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            siswa.setNamaSiswa(currentCell.getStringCellValue());
                            break;
                        case 1:
                            siswa.setKelas(currentCell.getStringCellValue());
                            break;
                        case 2:
                            siswa.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 3:
                            siswa.setTanggalLahir(currentCell.getDateCellValue());
                            break;
                        case 4:
                            siswa.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    siswaList.add(siswa);
                    cellIdx++;
                }
            }
            workbook.close();
            return siswaList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    public static List<Guru> excelToGuru(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Guru> guruList = new ArrayList<Guru>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Guru guru = new Guru();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            guru.setNamaGuru(currentCell.getStringCellValue());
                            break;
                        case 1:
                            guru.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 2:
                            guru.setTanggalLahir(currentCell.getDateCellValue());
                            break;
                        case 3:
                            guru.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    guruList.add(guru);
                    cellIdx++;
                }
            }
            workbook.close();
            return guruList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    public static List<Karyawan> excelToKaryawan(InputStream is) {

        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Karyawan> karyawanList = new ArrayList<Karyawan>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();

                Karyawan karyawan = new Karyawan();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIdx) {
                        case 0:
                            karyawan.setNamaKaryawan(currentCell.getStringCellValue());
                            break;
                        case 1:
                            karyawan.setTempatLahir(currentCell.getStringCellValue());
                            break;
                        case 2:
                            karyawan.setTanggalLahir(currentCell.getDateCellValue());
                            break;
                        case 3:
                            karyawan.setAlamat(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    karyawanList.add(karyawan);
                    cellIdx++;
                }
            }
            workbook.close();
            return karyawanList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    public static ByteArrayInputStream guruToExcel(List<Guru> guruList) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_GURU.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_GURU[col]);
            }

            int rowIdx = 1;
            for (Guru guru : guruList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(guru.getNamaGuru());
                row.createCell(1).setCellValue(guru.getTempatLahir());
                row.createCell(2).setCellValue(guru.getTanggalLahir());
                row.createCell(3).setCellValue(guru.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    public static ByteArrayInputStream siswaToExcel(List<Siswa> siswaList) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_SISWA.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_SISWA[col]);
            }

            int rowIdx = 1;
            for (Siswa siswa : siswaList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(siswa.getNamaSiswa());
                row.createCell(1).setCellValue(siswa.getKelas());
                row.createCell(2).setCellValue(siswa.getTempatLahir());
                row.createCell(3).setCellValue(siswa.getTanggalLahir());
                row.createCell(4).setCellValue(siswa.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    public static ByteArrayInputStream karyawanToExcel(List<Karyawan> karyawanList) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet(SHEET);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < HEADER_KARYAWAN.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(HEADER_KARYAWAN[col]);
            }

            int rowIdx = 1;
            for (Karyawan karyawan : karyawanList) {
                Row row = sheet.createRow(rowIdx++);

                row.createCell(0).setCellValue(karyawan.getNamaKaryawan());
                row.createCell(1).setCellValue(karyawan.getTempatLahir());
                row.createCell(2).setCellValue(karyawan.getTanggalLahir());
                row.createCell(3).setCellValue(karyawan.getAlamat());
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

}
