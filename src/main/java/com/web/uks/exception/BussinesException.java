package com.web.uks.exception;

public class BussinesException extends RuntimeException {
    public BussinesException(String message) {
        super(message);
    }
}
