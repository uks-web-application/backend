package com.web.uks.exception;

import com.web.uks.response.ResponseHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFoundAdviceController(NotFoundException notFoundException) {
        return ResponseHelper.errorResponse(HttpStatus.NOT_FOUND, notFoundException.getMessage());
    }
    @ExceptionHandler(BussinesException.class)
    public ResponseEntity<?> bussinesExceprionAdviceController(BussinesException bussinesException) {
        return ResponseHelper.errorResponse(HttpStatus.INTERNAL_SERVER_ERROR, bussinesException.getMessage());
    }
}
