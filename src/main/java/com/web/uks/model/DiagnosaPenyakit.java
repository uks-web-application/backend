package com.web.uks.model;

import com.web.uks.auditing.DateConfig;

import javax.persistence.*;

@Entity
@Table(name = "diagnosa_penyakit")
public class DiagnosaPenyakit extends DateConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
