package com.web.uks.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.web.uks.auditing.DateConfig;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "periksa")
public class Periksa extends DateConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_pasien")
    private String namaPasien;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pasien_status_id")
    private PasienStatus pasienStatusId;

    @Lob
    @Column(name = "keluhan")
    private String keluhan;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "keterangan_pasien_id")
    private KeteraganPasien keteraganPasienId;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "siswa_id")
    private Siswa siswaId;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawanId;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "guru_id")
    private Guru guruId;

    @Column(name = "status")
    private boolean handled;

    @Lob
    @Column(name = "daftar_penanganan")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String daftarPenanganan;

    @Transient
    private List<PenangananPeriksa> listPenanganan = new ArrayList<>();

    public Periksa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaPasien() {
        return namaPasien;
    }

    public void setNamaPasien(String namaPasien) {
        this.namaPasien = namaPasien;
    }

    public PasienStatus getPasienStatusId() {
        return pasienStatusId;
    }

    public void setPasienStatusId(PasienStatus pasienStatusId) {
        this.pasienStatusId = pasienStatusId;
    }

    public String getKeluhan() {
        return keluhan;
    }

    public void setKeluhan(String keluhan) {
        this.keluhan = keluhan;
    }

    public Siswa getSiswaId() {
        return siswaId;
    }

    public void setSiswaId(Siswa siswaId) {
        this.siswaId = siswaId;
    }

    public Karyawan getKaryawanId() {
        return karyawanId;
    }

    public void setKaryawanId(Karyawan karyawanId) {
        this.karyawanId = karyawanId;
    }

    public Guru getGuruId() {
        return guruId;
    }

    public void setGuruId(Guru guruId) {
        this.guruId = guruId;
    }

    public KeteraganPasien getKeteraganPasienId() {
        return keteraganPasienId;
    }

    public void setKeteraganPasienId(KeteraganPasien keteraganPasienId) {
        this.keteraganPasienId = keteraganPasienId;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

    public String getDaftarPenanganan() {
        return daftarPenanganan;
    }

    public void setDaftarPenanganan(String daftarPenanganan) {
        this.daftarPenanganan = daftarPenanganan;
    }

    public List<PenangananPeriksa> getListPenanganan() {
        return listPenanganan;
    }

    public void setListPenanganan(List<PenangananPeriksa> listPenanganan) {
        this.listPenanganan = listPenanganan;
    }
}
