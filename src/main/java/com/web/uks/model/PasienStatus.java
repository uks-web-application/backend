package com.web.uks.model;

import com.web.uks.auditing.DateConfig;

import javax.persistence.*;

@Entity
@Table(name = "pasien_status")
public class PasienStatus extends DateConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    public PasienStatus() {
    }

    public PasienStatus(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
