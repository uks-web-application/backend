package com.web.uks.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "penanganan_periksa")
public class PenangananPeriksa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "diagnosa_penyakit_id")
    private DiagnosaPenyakit diagnosaPenyakitId;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "keterangan_pasien_id")
    private KeteraganPasien keteraganPasienId;
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "penanganan_pertama_id")
    private PenangananPertama penangananPertamaId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "periksa_id")
    private Periksa periksaId;

    @Column(name = "catatan")
    private String catatan;

    public PenangananPeriksa() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DiagnosaPenyakit getDiagnosaPenyakitId() {
        return diagnosaPenyakitId;
    }

    public void setDiagnosaPenyakitId(DiagnosaPenyakit diagnosaPenyakitId) {
        this.diagnosaPenyakitId = diagnosaPenyakitId;
    }

    public KeteraganPasien getKeteraganPasienId() {
        return keteraganPasienId;
    }

    public void setKeteraganPasienId(KeteraganPasien keteraganPasienId) {
        this.keteraganPasienId = keteraganPasienId;
    }

    public PenangananPertama getPenangananPertamaId() {
        return penangananPertamaId;
    }

    public void setPenangananPertamaId(PenangananPertama penangananPertamaId) {
        this.penangananPertamaId = penangananPertamaId;
    }

    public Periksa getPeriksaId() {
        return periksaId;
    }

    public void setPeriksaId(Periksa periksaId) {
        this.periksaId = periksaId;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
