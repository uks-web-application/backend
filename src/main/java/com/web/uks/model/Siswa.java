package com.web.uks.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "siswa")
public class Siswa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama_siswa")
    private String namaSiswa;

    @Column(name = "kelas")
    private String kelas;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "tanggal_lahir")
    private Date tanggalLahir;

    @Column(name = "alamat")
    private String alamat;

    @Column(name =  "total_periksa")
    private int totalPeriksa = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaSiswa() {
        return namaSiswa;
    }

    public void setNamaSiswa(String namaSiswa) {
        this.namaSiswa = namaSiswa;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getTotalPeriksa() {
        return totalPeriksa;
    }

    public void setTotalPeriksa(int totalPeriksa) {
        this.totalPeriksa = totalPeriksa;
    }
}
